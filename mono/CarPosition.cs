using System;

public class CarPosition
{
	public CarId id;
	public float angle;
	public CarPiecePosition piecePosition;
	public Lane lane;
	public int lap;
}

