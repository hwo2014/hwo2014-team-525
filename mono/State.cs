using System;

public abstract class State
{
	public abstract void Think(Race race, CarId ourCarId);
	
	public abstract SendMsg Act();

	public abstract StateType NextState();
}

