using System;

public class CalculateTurboState : State
{
	Turbo turbo;
	StateType nextState;
	
	public override void Think(Race race, CarId ourCarId) 
	{
		turbo = null;

		Car ourCar = race.GetCar(ourCarId);

		// TODO
		bool shouldUseTurbo = race.LongStraightComing(ourCar) || race.IsOpponentBlocking(ourCar);

		if (shouldUseTurbo)
		{
			turbo = race.ConsumeTurbo();
		}

		nextState = StateType.CalculateThrottle;
	}
	
	public override SendMsg Act()
	{
		if (turbo == null)
		{
			return new Ping();
		}
		return turbo;
	}
	
	public override StateType NextState()
	{
		return nextState;
	}
}
