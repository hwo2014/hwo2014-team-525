using System;

public class Race
{
	public int currentGameTick;
	public Track track;
	public Car[] cars;

	//TODO get raceSession data
	public Object raceSession;

	public Turbo turbo;
	private int turboTriggeredAtTick = -1;


	public void Initialize()
	{
		track.Initialize();
	}

	public TurnType CalculateNextSwitchDirection(int currentIdPiece, int currentIdLane)
	{
		return track.CalculateNextSwitchDirection(currentIdPiece, currentIdLane);
	}

	public Car GetCar(CarId carId) 
	{
		foreach (Car car in cars) 
		{
			if (car.id.color == carId.color)
			{
				return car;
			}
		}
		return null;
	}

	public void UpdateRace(CarPosition[] carPositions, int gameTick)
	{
		currentGameTick = gameTick;
		UpdateTurboStatus();

		foreach (CarPosition carPosition in carPositions) 
		{
			GetCar(carPosition.id).UpdatePosition(carPosition, track);
		}
	}

	public void UpdateCrashForCar(CarId carId)
	{
		GetCar(carId).Crash(track);
	}

	public void UpdateThrottleForCar(CarId carId, float throttle)
	{
		GetCar(carId).currentThrottle = throttle;
	}

	
	public void DebugVelocityForCars()
	{
		foreach (Car car in cars) 
		{
			car.DebugVelocity();
		}
	}

	public void DebugAccelerationForCars()
	{
		foreach (Car car in cars) 
		{
			car.DebugAcceleration();
		}
	}


	public void AddTurbo(Turbo turbo) 
	{
		this.turbo = turbo;
	}

	public bool TurboAvailable()
	{
		return turbo != null && !ConsumingTurbo();
	}

	public bool ConsumingTurbo()
	{
		if (turbo == null)
		{
			return false;
		}
		return turboTriggeredAtTick > 0 && (currentGameTick <= turboTriggeredAtTick + turbo.turboDurationTicks) && (currentGameTick >= turboTriggeredAtTick);
	}

	public Turbo ConsumeTurbo()
	{
		turboTriggeredAtTick = currentGameTick;
		return turbo;
	}

	public void UpdateTurboStatus()
	{
		if (turbo == null || turboTriggeredAtTick < 0)
		{
			return;
		}

		if (!ConsumingTurbo())
		{
			turboTriggeredAtTick = -1;
			turbo = null;
		}

	}

	public bool IsSwitch(int idPiece)
	{
		return track.IsSwitch(idPiece);
	}

	public bool IsOpponentBlocking(Car ourCar)
	{
		foreach (Car car in cars) {
			if (car.currentPosition.lane == ourCar.currentPosition.lane &&
				((car.currentPosition.pieceIndex == ourCar.currentPosition.pieceIndex && car.currentPosition.inPieceDistance > ourCar.currentPosition.inPieceDistance) ||
				(car.currentPosition.pieceIndex == (ourCar.currentPosition.pieceIndex + 1)%track.numberOfPieces))) 
			{
				return true;
			}
		}
		return false;
	}

	public bool LongStraightComing(Car ourCar)
	{
		int currentPiece = ourCar.currentPosition.pieceIndex;
		if (PieceIsStraight(currentPiece) &&
		    PieceIsStraight((currentPiece+1)%track.numberOfPieces) &&
		    PieceIsStraight((currentPiece+2)%track.numberOfPieces) &&
		    PieceIsStraight((currentPiece+3)%track.numberOfPieces)) 
		{
			return true;

		}
		return false;
	}

	public bool PieceIsStraight(int idPiece)
	{
		return track.pieces[idPiece].length > 0f;
	}
}

