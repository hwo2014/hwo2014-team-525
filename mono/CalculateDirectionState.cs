using System;

public class CalculateDirectionState : State
{
	TurnType nextDirection;
	StateType nextState;

	public override void Think(Race race, CarId ourCarId) 
	{
		Car ourCar = race.GetCar(ourCarId);

		nextDirection = race.CalculateNextSwitchDirection(ourCar.currentPosition.pieceIndex, ourCar.currentPosition.lane.startLaneIndex);
		//Console.WriteLine("DIR"+nextDirection.ToString()+" index+"+(ourCar.currentPosition.pieceIndex)+ " lane"+ourCar.currentPosition.lane.index);
		nextState = StateType.CalculateThrottle;
	}
	
	public override SendMsg Act()
	{
		if (nextDirection == TurnType.Straight)
		{
			return new Ping();
		}
		return new SwitchMsg(nextDirection);
	}
	
	public override StateType NextState()
	{
		return nextState;
	}
}

