
public class SwitchNode
{
	public int id;
	public int idLane;
	public int idPiece;

	public SwitchNode left;
	public SwitchNode straight;
	public SwitchNode right;

	public float weight;
	public float accumulatedWeight;
	public SwitchNode previousNode;
	public int weightIterationIndex;

	public SwitchNode (int id, int idLane, int idPiece)
	{
		this.id = id;
		this.idLane = idLane;
		this.idPiece = idPiece;
	}

	public void InitWeight()
	{
		weight = float.MaxValue;
		accumulatedWeight = 0;
		previousNode = null;
	}
}

