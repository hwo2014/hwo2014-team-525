using System;

public class Turbo : SendMsg
{
	public float turboDurationMilliseconds;
	public int turboDurationTicks;
	public float turboFactor;
	

	protected override Object MsgData() {
		return "TURBO!";
	}
	
	protected override string MsgType() {
		return "turbo";
	}
}

