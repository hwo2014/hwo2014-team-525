using System;
using System.Collections;
using System.Collections.Generic;


public class Track
{
	public string id;
	public string name;
	public Piece[] pieces;
	public List<Lane> lanes;
	public Object startingPoint;
		
	private List<SwitchPiece> switchPieces;

	public int numberOfPieces { get { return pieces.Length; } }
	
	public void Initialize() 
	{
		// Sort lanes by distanceFromCenter to ensure correct switch connections between lanes
		lanes.Sort (
			delegate(Lane l1, Lane l2) 
			{
				return l1.distanceFromCenter.CompareTo(l2.distanceFromCenter);
			}
		);

		// Get switch pieces
		int idNode = 0;
		int idPiece = 0;
		switchPieces = new List<SwitchPiece>();
		foreach (Piece piece in pieces) 
		{
			if (!piece.@switch)
			{
				idPiece++;
				continue;
			}

			List<SwitchNode> nodes = new List<SwitchNode>();
			foreach (Lane lane in lanes)
			{
				nodes.Add(new SwitchNode(idNode, lane.index, idPiece));
				idNode++;
			}
			switchPieces.Add(new SwitchPiece(idPiece, nodes));
			idPiece++;
		}

		// Connect switch pieces
		int numberOfSwitchPieces = switchPieces.Count;
		if (numberOfSwitchPieces > 0) 
		{
			switchPieces[numberOfSwitchPieces - 1].Connect(switchPieces [0]);
			for (int i = 0; i < numberOfSwitchPieces - 1; i++) 
			{
				switchPieces[i].Connect(switchPieces[i + 1]);
			}
		}

		// Sort lanes by index for future access
		lanes.Sort (
			delegate(Lane l1, Lane l2) 
			{
				return l1.index.CompareTo(l2.index);
			}
		);

		DebugSwitchesDistances();
	}


	private void DebugSwitches() 
	{
		foreach (SwitchPiece piece in switchPieces) 
		{
			Console.WriteLine("Piece: "+piece.idPiece);
			List<SwitchNode> nodes = piece.nodes;
			foreach(SwitchNode node in nodes) 
			{
				Console.WriteLine("Lane: "+node.idLane);
				if (node.left != null) 
				{
					Console.WriteLine("Transition: "+node.id+"<-"+node.left.id);
				}
				if (node.straight != null) 
				{
					Console.WriteLine("Transition: "+node.id+"^"+node.straight.id);
				}
				if (node.right != null) 
				{
					Console.WriteLine("Transition: "+node.id+"->"+node.right.id);
				}
			}
		}
	}

	private void DebugSwitchesDistances() 
	{
		foreach (SwitchPiece piece in switchPieces) 
		{
			Console.WriteLine("Piece: "+piece.idPiece);

			List<SwitchNode> nodes = piece.nodes;
			foreach(SwitchNode node in nodes) 
			{
				Console.WriteLine("Lane: "+node.idLane);
				SwitchNode nextNode = GetNextSwitch(piece.idPiece+1,node.idLane);
				float weight = GetWeight(node.idPiece, nextNode.idPiece, lanes[node.idLane].distanceFromCenter, lanes[nextNode.idLane].distanceFromCenter);
				
				Console.WriteLine("From: "+node.id+ " to:"+nextNode.id + " - "+ weight);

			}
		}
	}

	// Next switch node given current idPiece and lane
	private SwitchNode GetNextSwitch(int currentIdPiece, int currentIdLane)
	{
		foreach (SwitchPiece piece in switchPieces) 
		{
			if (currentIdPiece < piece.idPiece)
			{
				return piece.GetSwitchNode(currentIdLane);
			}
		}
		return switchPieces[0].GetSwitchNode(currentIdLane);
	}

	public bool IsSwitch(int idPiece)
	{
		return pieces[idPiece].@switch;
	}

	// TODO: Currently weight is only based on distance between nodes. Depending on friction we could run faster on the exterior (or middle) lane...
	// Ideally this would calculate time between nodes at max speed
	// Can friction be different on each piece or is it the same for the whole track?
	private float GetWeight(int fromPiece, int toPiece, float startDistanceFromCenter, float endDistanceFromCenter)
	{
		float length = 0;
		// Last section
		if (toPiece < fromPiece) 
		{
			toPiece += pieces.Length;
		}
		for (int i = fromPiece; i < toPiece; i++) 
		{
			int j = i%pieces.Length;
			length += pieces[j].GetLength(startDistanceFromCenter, endDistanceFromCenter);
		}
		return length;
	}

	public TurnType CalculateNextSwitchDirection(int currentIdPiece, int currentIdLane)
	{
		SwitchNode root = GetNextSwitch(currentIdPiece, currentIdLane);

		foreach (SwitchPiece piece in switchPieces) 
		{
			piece.InitWeights();
		}

		List<SwitchNode> queue = new List<SwitchNode>();
		List<SwitchNode> queue2 = new List<SwitchNode>();
		int index = 0;
		CalculateChildrenWeights(root);


		if (root.left != null) queue.Add(root.left);
		if (root.straight != null) queue.Add(root.straight);
		if (root.right != null) queue.Add(root.right);

		foreach(SwitchNode switchNode in queue)
		{
			switchNode.accumulatedWeight = switchNode.weight;
			switchNode.weight = float.MaxValue;
		}

		while(queue.Count > 0)
		{
			SwitchNode node = queue[index];
			index++;

			CalculateChildrenWeights(node);

			if (queue.Count <= index)
			{
				queue2.Clear();
				foreach(SwitchNode switchNode in queue)
				{
					switchNode.accumulatedWeight = switchNode.weight;
					switchNode.weight = float.MaxValue;

					if (IsRelevant(switchNode, switchNode.left)) queue2.Add(switchNode.left);
					if (IsRelevant(switchNode, switchNode.straight)) queue2.Add(switchNode.straight);
					if (IsRelevant(switchNode, switchNode.right)) queue2.Add(switchNode.right);
				}
				foreach(SwitchNode endNode in queue2) 
				{
					if (endNode.id == root.id) 
					{
						SwitchNode parentNode = endNode.previousNode;
						SwitchNode nextNode = endNode;
						while (parentNode.id != root.id)
						{
							nextNode = parentNode;
							parentNode = nextNode.previousNode;
						}
						return GetDirection(root, nextNode);
					}
				}

				queue.Clear();
				queue.AddRange(queue2);

				index = 0;
			}
		}
		return TurnType.Straight;
	}

	private void CalculateChildrenWeights(SwitchNode node)
	{
		CalculateChildWeight(node, node.left);
		CalculateChildWeight(node, node.straight);
		CalculateChildWeight(node, node.right);
	}

	private void CalculateChildWeight(SwitchNode parent, SwitchNode child)
	{
		if (child == null)
		{
			return;
		}
		float weight = GetWeight(parent.idPiece, child.idPiece, lanes[parent.idLane].distanceFromCenter, lanes[child.idLane].distanceFromCenter);
		if (child.weight > weight + parent.accumulatedWeight)
		{
			child.weight = weight + parent.accumulatedWeight;
			child.previousNode = parent;
		}
	}

	private bool IsRelevant(SwitchNode parent, SwitchNode child)
	{
		if (child == null)
		{
			return false;
		}
		float weight = GetWeight(parent.idPiece, child.idPiece, lanes[parent.idLane].distanceFromCenter, lanes[child.idLane].distanceFromCenter);
		return child.weight <= (weight + parent.accumulatedWeight);
	}

	private TurnType GetDirection(SwitchNode node, SwitchNode nextNode)
	{
		if (node.left != null)
		{
			if (node.left.id == nextNode.id)
				return TurnType.Left;
		}
		if (node.right != null)
		{
			if (node.right.id == nextNode.id)
				return TurnType.Right;
		}
		return TurnType.Straight;
	}

}


