using System;
using System.Collections.Generic;

public class Car
{
	public CarId id;
	public CarDimensions dimensions;
	public float angle;
	public CarPiecePosition currentPosition;
	public CarPiecePosition previousPosition;
	public float previousVelocity;
	public float currentVelocity;
	public float currentThrottle;
	public float currentAcceleration;
	public float previousAcceleration;
	public float realFriction;
	public float frictionCoeficient;
	public float decelerationRate;
	public float velocityForDecelerationRate;
	public bool calculateDecelerationRate = false;

	//DEBUG
	public float debugTopSpeed;
	public float debugAvgSpeed;
	public float debugTopAcceleration;
	public float debugAvgAcceleration;
	public float debugMinAcceleration;
	public int debugNumSampleSpeed;
	public int debugNumSampleAcceleration;

	private const float GRAVITY = 9.8f;
	private const float VELOCITY_DELECERATION_RATE = 1.0f;


	// Quizas velocidad deberia ser Vector2(x, y) para saber en las curvas que componente hace que el coche se salga (con throttle aumentara una mas que la otra)
	public float GetCurrentVelocity(Track track)
	{
		float velocity = 0.0f;
		//TODO: If exists previous and current calculate velocity
		if (previousPosition != null)
		{
			//si current es mayor obtener la resta de las posiciones para saber la velocidad en pixel x tick
			int currentPieceIndex = currentPosition.pieceIndex;
			int previousPieceIndex = previousPosition.pieceIndex;
			if ( previousPieceIndex == currentPieceIndex) {
				velocity = currentPosition.inPieceDistance - previousPosition.inPieceDistance;
			}
			//If previous position was in a previous piece
			else {
				float previousLength = track.pieces[previousPieceIndex].GetExactLength(0,previousPosition.lane.startLaneIndex, previousPosition.lane.endLaneIndex,track);
				//Console.WriteLine("Previous lenght: "+previousLength);
				//Console.WriteLine("Previous Piece: "+ previousPosition.ToString());
				//Console.WriteLine("Current Piece: "+ currentPosition.ToString());
				velocity = previousLength - previousPosition.inPieceDistance + currentPosition.inPieceDistance;
			}
			Console.WriteLine("Velocity: "+ velocity);

			//Debug
			if (velocity > debugTopSpeed) debugTopSpeed = velocity;
			debugNumSampleSpeed++;
			debugAvgSpeed = (debugAvgSpeed*(debugNumSampleSpeed-1)+velocity)/debugNumSampleSpeed;
		}
		return velocity;
	}

	public float GetCurrentAcceleration()
	{
		float acceleration = currentVelocity - previousVelocity;
		if (acceleration > debugTopAcceleration) debugTopAcceleration = acceleration;
		if (acceleration < debugMinAcceleration) debugMinAcceleration = acceleration;
		debugNumSampleAcceleration++;
		debugAvgAcceleration = (debugAvgAcceleration*(debugNumSampleAcceleration-1)+acceleration)/debugNumSampleAcceleration;
		return acceleration;
	}
	//Not real friction, only a value defining the carachteristics of the track
	public float GetEstimatedFriction()
	{
		float friction = 0.0f;
		//If we have a really aproximated friction use it
		if (realFriction != 0) {
			friction = realFriction;
			Console.WriteLine("Real Friction: "+friction);
		}
		//If constant velocity Throttle = Friction
		else if (previousVelocity > 0 && currentVelocity > 0 && currentThrottle == 1.0f && previousAcceleration > 0 && currentAcceleration > 0 && realFriction == 0)
		{
			//friction = currentAcceleration - previousAcceleration;
			friction = (2*(previousVelocity-currentVelocity))/(previousVelocity);
			//friction = previousAcceleration;
			realFriction = friction;
			Console.WriteLine("Found Friction: "+realFriction);
		}
		else Console.WriteLine("Waiting for determining friction");
		return friction;
	}

	public float GetCurrentFrictionEffect()
	{
		return frictionCoeficient*currentVelocity*currentVelocity;
	}

	public void CalculateDecelerationRate()
	{
		if (currentAcceleration < 0 && previousAcceleration < 0) {
			decelerationRate = currentAcceleration;
			velocityForDecelerationRate = currentVelocity;		
			calculateDecelerationRate = false;
		}
	}

	public void UpdatePosition(CarPosition position, Track track)
	{
		previousPosition = currentPosition;
		currentPosition = position.piecePosition;
		Console.WriteLine("Current Piece: " + currentPosition.pieceIndex +"; " + track.pieces[currentPosition.pieceIndex].ToString());
		previousVelocity = currentVelocity;
		currentVelocity = GetCurrentVelocity(track);
		previousAcceleration = currentAcceleration;
		currentAcceleration = GetCurrentAcceleration();
		Console.WriteLine("Current throttle: " + currentThrottle);
		Console.WriteLine("Current angle: " + angle);
		//Console.WriteLine("Previous Acceleration: "+previousAcceleration);
		Console.WriteLine("Current Acceleration: "+currentAcceleration);		
		Console.WriteLine("Acceleration Difference: "+Math.Abs(currentAcceleration-previousAcceleration));
		Console.WriteLine("Coef Friction: "+frictionCoeficient);
		if (calculateDecelerationRate) {
			CalculateDecelerationRate();
		}
		Console.WriteLine("Deceleration rate: " + decelerationRate);
		Console.WriteLine("Current velocity for deceleration rate: " + velocityForDecelerationRate);
		//currentFriction = GetEstimatedFriction();
		//float frictionEffect = GetCurrentFrictionEffect();
		//Console.WriteLine("Current Friction effect: "+frictionEffect);
		angle = position.angle;
	}

	public void Crash(Track track)
	{
		//Calculate friction coef: Vmax = sqrt(radius*gravity*coefriction)
		float radius = track.pieces[currentPosition.pieceIndex].radius;
		if (radius != 0)
		{
			frictionCoeficient = (currentVelocity*currentVelocity)/(radius*GRAVITY);
			Console.WriteLine("Coef Found: "+frictionCoeficient);
		}
		previousVelocity = 0;
		previousAcceleration = 0;
		currentVelocity = 0;
		currentAcceleration = 0;
		Console.WriteLine("¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡CRASH!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
	}

	private float GetLenghtBetweenPositions(CarPiecePosition from, int to, Track track)
	{
		bool remainingIndexs = true;
		int fromIndex = from.pieceIndex;
		int toIndex = to;
		if (fromIndex == toIndex) return 0;
		else {
			float lenght = 0.0f;
			while (fromIndex != toIndex)
			{
				lenght += track.pieces[fromIndex].GetExactLength(0,from.lane.index,from.lane.index,track);
				fromIndex = fromIndex + 1 >= track.pieces.Length ? 0 : fromIndex + 1;
			}
			return lenght - currentPosition.inPieceDistance;
		}

	}

	private List<float> GetTopSpeedForNextCurve(Track track)
	{
		int currentPieceIndex = currentPosition.pieceIndex;
		bool startingInCurve = track.pieces[currentPieceIndex].radius != 0;
		bool curveFound = false;
		while (!curveFound) {
			Console.WriteLine("CurrentIndex: " + currentPieceIndex);
			if (startingInCurve) {
				Console.WriteLine("Curve");
				currentPieceIndex = currentPieceIndex + 1 >= track.pieces.Length ? 0 : currentPieceIndex + 1;
				if (track.pieces[currentPieceIndex].radius == 0) startingInCurve = false;
			}
			else {				
				Console.WriteLine("No Curve");
				currentPieceIndex = currentPieceIndex + 1 >= track.pieces.Length ? 0 : currentPieceIndex + 1;
				if (track.pieces[currentPieceIndex].radius != 0) curveFound = true;
			}
		}
		float topSpeed = (float)Math.Sqrt(GRAVITY*frictionCoeficient*track.pieces[currentPieceIndex].radius);
		float distance = GetLenghtBetweenPositions(currentPosition,currentPieceIndex,track);
		Console.WriteLine("TopSpeed: " +topSpeed);
		Console.WriteLine("Distance: " +distance);
		List<float> returning = new List<float>();
		returning.Add(topSpeed);
		returning.Add(distance);
		return returning;
	}

	public float GetDesiredThrottle(Track track){
		float nextTrhottle = currentThrottle;
		if (track.pieces[currentPosition.pieceIndex].radius != 0 ) {
			Console.WriteLine("In curve");
			if (currentAcceleration - previousAcceleration > 0.01f){
				nextTrhottle = currentThrottle - 0.1f;
			}
			else if (currentAcceleration - previousAcceleration < 0.01f){				
				nextTrhottle = currentThrottle + 0.1f;
			} //Mantain constant acceleration
		}
		else {
			Console.WriteLine("Getting");
			List<float> topSpeedAndDistance = GetTopSpeedForNextCurve(track);
			Console.WriteLine("TopSpeedAndDistance: ",topSpeedAndDistance);

			float topSpeed = topSpeedAndDistance[0];
			float distance = topSpeedAndDistance[1];
			float desiredAcceleration = (topSpeed*topSpeed-currentVelocity*currentVelocity)/(2*distance);
			if (desiredAcceleration <= decelerationRate) nextTrhottle = currentThrottle - Math.Abs (velocityForDecelerationRate-currentVelocity);
		}		
		nextTrhottle = nextTrhottle < 0.0f ? 0.0f : (nextTrhottle > 1.0 ? 1.0f : nextTrhottle);
		Console.WriteLine("Next Throttle: ",nextTrhottle);
		return nextTrhottle;		
	}

	public void DebugVelocity()
	{
		Console.WriteLine("TopSpeed: "+debugTopSpeed);
		Console.WriteLine("AvgSpeed: "+ debugAvgSpeed);
	}
		
	public void DebugAcceleration()
	{
		Console.WriteLine("TopAcceleration: "+debugTopAcceleration);
		Console.WriteLine("MinAcceleration: "+debugMinAcceleration);
		Console.WriteLine("AvgAcceleration: "+ debugAvgAcceleration);
	}

}

public class CarId
{
	public string name;
	public string color;

	public override string ToString()
	{
		return "Car Name: " + name + "; Car Color: " + color;
	}
}

public class CarDimensions
{
	public float length;
	public float width;
	public float guideFlagPosition;
}

public class CarPiecePosition
{
	public int lap;
	public int pieceIndex;
	public float inPieceDistance;
	public Lane lane;

	public override string ToString()
	{
		return "Lap: " + lap + "; Piece Index: " + pieceIndex + "; In Piece Distance: " + inPieceDistance + "; Lane: " + lane.ToString();
	}
}
