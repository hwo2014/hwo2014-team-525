using System;

class MsgWrapper {
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data) {
    	this.msgType = msgType;
    	this.data = data;
    }
}

public class MsgWrapperTick 
{
	public string msgType;
	public Object data;
	public int gameTick;
}