using System;

public class CalculateThrottleState : State
{
	float nextThrottle;
	StateType nextState;
	ThrottleStateType currentTrhottleState = ThrottleStateType.CalculateTopCurveSpeed;
	
	public override void Think(Race race, CarId ourCarId) 
	{
		Car ourCar = race.GetCar(ourCarId);

		float turboFactor = race.ConsumingTurbo() ? race.turbo.turboFactor : 1f;

		Console.WriteLine("gameTick on race: "+race.currentGameTick);


		if (ourCar.frictionCoeficient != 0 && currentTrhottleState == ThrottleStateType.CalculateTopCurveSpeed ) currentTrhottleState = ThrottleStateType.CalculateMaxDeceleration;


		Console.WriteLine("Current Throttle State: "+currentTrhottleState);





		if (ourCar.frictionCoeficient == 0 && currentTrhottleState == ThrottleStateType.CalculateTopCurveSpeed) {
			nextThrottle = 1.0f;
			Console.WriteLine("STATE MACHINE: Looking for friction");
		}
		//If we are looking for deceleration estimation we want to be in a straigth piece
		else if (currentTrhottleState == ThrottleStateType.CalculateMaxDeceleration && race.track.pieces[ourCar.currentPosition.pieceIndex].radius != 0 && ourCar.angle <= 10.0f) {
			nextThrottle = 1.0f;
			Console.WriteLine("STATE MACHINE: Looking for straitght lane at full throttle");
		}
		else if (currentTrhottleState == ThrottleStateType.CalculateMaxDeceleration && race.track.pieces[ourCar.currentPosition.pieceIndex].radius == 0 && ourCar.angle <= 10.0f) {
			nextThrottle = 0.0f;
			ourCar.calculateDecelerationRate = true;
			currentTrhottleState = ThrottleStateType.CalculateCurrentThrottle;
			Console.WriteLine("STATE MACHINE: Looking for deceleration ratee");
		}
		else if (currentTrhottleState == ThrottleStateType.CalculateCurrentThrottle)
		{
			nextThrottle = ourCar.GetDesiredThrottle(race.track);
		}

		else if (race.currentGameTick >= 700) {
			nextThrottle = 0.0f;
		}
		else nextThrottle = 0.6f;


		bool inSwitchPiece = race.IsSwitch(ourCar.currentPosition.pieceIndex);


		race.UpdateThrottleForCar(ourCarId,nextThrottle);

		nextState = inSwitchPiece ? StateType.CalculateDirection : (race.TurboAvailable() ? StateType.CalculateTurbo : StateType.CalculateThrottle);

	}
	
	public override SendMsg Act()
	{
		return new Throttle(nextThrottle);
	}

	public override StateType NextState()
	{
		return nextState;
	}
}

public enum ThrottleStateType 
{
	CalculateTopCurveSpeed,
	CalculateMaxDeceleration,
	CalculateCurrentThrottle
}


