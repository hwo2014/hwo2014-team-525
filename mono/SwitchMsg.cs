using System;

public class SwitchMsg : SendMsg
{
	public TurnType turnType;

	public SwitchMsg(TurnType value) {
		this.turnType = value;
	}
	
	protected override Object MsgData() {
		return this.turnType.ToString();
	}
	
	protected override string MsgType() {
		return "switchLane";
	}
}