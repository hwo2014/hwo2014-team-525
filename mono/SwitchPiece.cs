using System;
using System.Collections.Generic;

public class SwitchPiece
{
	public int idPiece;
	public List<SwitchNode> nodes;

	public SwitchPiece(int idPiece, List<SwitchNode> nodes)
	{
		this.idPiece = idPiece;
		this.nodes = nodes;
	}

	public void Connect(SwitchPiece nextPiece)
	{
		int numberOfNodes = nodes.Count;
		List<SwitchNode> nextNodes = nextPiece.nodes;

		for (int j = 0; j < numberOfNodes; j++)
		{
			nodes[j].straight = nextNodes[j];
			if (j > 0)
			{
				nodes[j].left = nextNodes[j-1];
			}
			if (j < numberOfNodes - 1)
			{
				nodes[j].right = nextNodes[j+1];
			}
		}
	}

	public SwitchNode GetSwitchNode(int idLane) 
	{
		foreach (SwitchNode node in nodes) 
		{
			if (node.idLane == idLane)
			{
				return node;
			}
		}
		return null;
	}

	public void InitWeights()
	{
		foreach (SwitchNode node in nodes) 
		{
			node.InitWeight();
		}
	}
}

