using System;

public class Piece
{
	public float length;
	public float radius;
	public float angle;
	public bool @switch;

	private const float PI = 3.141592f;

	public float getRadius(float radiusOffset) 
	{
		return radius + radiusOffset;
	}

	public float GetLength(float startDistanceFromCenter, float endDistanceFromCenter) 
	{
		if (startDistanceFromCenter != endDistanceFromCenter && @switch)
		{
			return length > 0f ? (float) Math.Sqrt( Math.Pow ( Math.Abs (startDistanceFromCenter - endDistanceFromCenter),2) + length*length)
				: (Math.Abs(angle*PI/180f) * (Math.Sign(-angle)*startDistanceFromCenter + radius) + Math.Abs(angle*PI/180f) * (Math.Sign(-angle)*endDistanceFromCenter + radius))/2;
		}
		else 
		{
			return length > 0f ? length : Math.Abs(angle*PI/180f) * (Math.Sign(-angle)*endDistanceFromCenter + radius);	
		}
	}

	public float GetExactLength(float radiusOffset, int startLaneIndex, int endLaneIndex, Track track) 
	{
		if (length > 0f)
		{
			return startLaneIndex != endLaneIndex ? (float) Math.Sqrt( Math.Pow ( Math.Abs (track.lanes[startLaneIndex].distanceFromCenter - track.lanes[endLaneIndex].distanceFromCenter),2) + length*length) : length;
		}
		else {
			return startLaneIndex != endLaneIndex ? (Math.Abs(angle*PI/180f) * (Math.Sign(-angle)*track.lanes[startLaneIndex].distanceFromCenter + radius) + Math.Abs(angle*PI/180f) * (Math.Sign(-angle)*track.lanes[endLaneIndex].distanceFromCenter + radius))/2 : Math.Abs(angle*PI/180f) * (Math.Sign(-angle)*radiusOffset + radius);

		}
	}

	public override string ToString ()
	{
		return string.Format ("Lenght: "+length+"; Radius: "+radius+"; Angle: "+angle+"; Switch: "+@switch+";");
	}
}

