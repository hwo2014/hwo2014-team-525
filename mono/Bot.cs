using System;
using System.IO;
using System.Net.Sockets;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Bot {
	public static void Main(string[] args) {
		//
		//string j = "{\"msgType\":\"gameInit\",\"data\":{\"race\":{\"track\":{\"id\":\"keimola\",\"name\":\"Keimola\",\"pieces\":[{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":62.0},{\"radius\":100,\"angle\":-45.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":100.0,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":90.0}],\"lanes\":[{\"distanceFromCenter\":-10,\"index\":0},{\"distanceFromCenter\":10,\"index\":1}],\"startingPoint\":{\"position\":{\"x\":-300.0,\"y\":-44.0},\"angle\":90.0}},\"cars\":[{\"id\":{\"name\":\"Offtopic\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":3,\"maxLapTimeMs\":60000,\"quickRace\":true}}},\"gameId\":\"a3ba87f2-097f-478e-99d4-ba353d7c0e95\"}";
		//MsgWrapper msgw = JsonConvert.DeserializeObject<MsgWrapper>(j);
		//Race racer = ((JObject)msgw.data).Value<JObject>("race").ToObject<Race>();
		//racer.Initialize();
		//Console.WriteLine (racer.CalculateNextSwitchDirection (1, 0));
		//return;
		//
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

			new Bot(reader, writer, new Join(botName, botKey));
		}
	}

	private StreamWriter writer;
	private StateManager stateManager;
	private Race race;
	private CarId ourCarId;

	Bot(StreamReader reader, StreamWriter writer, Join join) {
		this.writer = writer;
		string line;

		stateManager = new StateManager();

		send(join);

		while((line = reader.ReadLine()) != null) {
			MsgWrapperTick msg = JsonConvert.DeserializeObject<MsgWrapperTick>(line);
			//Console.WriteLine(line);
			switch(msg.msgType) {
				case "join":
					Console.WriteLine("Joined");
					send(new Ping());
					break;
				case "yourCar":
					Console.WriteLine("Your Car");
					ourCarId = ((JObject)msg.data).ToObject<CarId>();
					send(new Ping());
					break;
				case "gameInit":
					Console.WriteLine("Race init");
					race = ((JObject)msg.data).Value<JObject>("race").ToObject<Race>();
					race.Initialize();
				    //Console.WriteLine("Track Pieces" + race.track.pieces);
					send(new Ping());
					break;
				case "carPositions":
					Console.WriteLine("Car Positions.  Tick:"+msg.gameTick);

					CarPosition[] carPositions = ((JArray)msg.data).ToObject<CarPosition[]>();
					race.UpdateRace(carPositions, msg.gameTick);
					int gameTick = Convert.ToInt32(msg.gameTick);
					Console.WriteLine("GameTick: "+gameTick);
					//race.gameTick = gameTick;
					//race.UpdateCarPositions(carPositions);
					
					stateManager.Think(race, ourCarId);

					send(stateManager.Act());

					stateManager.CheckStateTransition();

					break;
				case "crash":
					// TODO
					
					CarId carId = ((JObject)msg.data).ToObject<CarId>();
					Console.WriteLine("Crashing: " + carId.ToString());
					race.UpdateCrashForCar(carId);
					send(new Ping());
					break;
				case "turboAvailable":
					Turbo turbo = ((JObject)msg.data).ToObject<Turbo>();
					Console.WriteLine("Turbo available: " + turbo.turboFactor);
					race.AddTurbo(turbo);
					send(new Ping());
					break;
				case "spawn":
					// TODO
					send(new Ping());
					break;
				case "dnf":
					Console.WriteLine("Car disqualified");
					send(new Ping());
					break;
				case "gameEnd":
					race.DebugVelocityForCars();
					race.DebugAccelerationForCars();
					Console.WriteLine("Race ended");
					send(new Ping());
					break;
				case "gameStart":
					Console.WriteLine("Race starts");
					send(new Ping());
					break;
				default:
					send(new Ping());
					break;
			}
		}
	}

	private void send(SendMsg msg) {
		//Console.WriteLine ("Response:" + msg.ToJson ());
		writer.WriteLine(msg.ToJson());
	}
}