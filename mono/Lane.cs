
public class Lane
{
	public float distanceFromCenter;
	public int index;
	public int startLaneIndex;
	public int endLaneIndex;

	public override string ToString()
	{
		return "Distance From Center: " + distanceFromCenter + " Index: " + index + "  Start Lane Index: " + startLaneIndex + " End Lane Index: " + endLaneIndex;
	}
}


