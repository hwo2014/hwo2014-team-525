using System;

public class StateManager
{
	private State currentState;

	private CalculateDirectionState directionState;
	private CalculateThrottleState throttleState;
	private CalculateTurboState turboState;

	
	public StateManager() 
	{
		directionState = new CalculateDirectionState();
		throttleState = new CalculateThrottleState();
		turboState = new CalculateTurboState();

		currentState = directionState;
	}

	public void Think(Race race, CarId ourCarId) 
	{
		currentState.Think(race, ourCarId);
	}

	public SendMsg Act()
	{
		return currentState.Act();
	}

	public void CheckStateTransition()
	{
		switch(currentState.NextState())
		{
			case StateType.CalculateDirection:
				currentState = directionState;
				break;

			case StateType.CalculateThrottle:
				currentState = throttleState;
				break;

			case StateType.CalculateTurbo:
				currentState = turboState;
				break;

			default:
				Console.WriteLine("Unknown state: "+currentState.NextState());
				break;
		}
	}
}

public enum StateType 
{
	CalculateDirection,
	CalculateThrottle,
	CalculateTurbo
}

